# Introduction:

Vibra Voice is a Max For Live plugin that adds and transforms the vibrato of a singing voice, in real time and automatically. Vibra Voice offers several possibilities to modulate a melody, in the studio or on the stage. First of all, Vibra Voice allows the automatic correction of the “correctness/tuning” of a melodic line as well as the amplification / suppression of its pitch modulation. In the context of the singing voice, Vibra Voice works as a high quality "autotune" (the vocoder effect is minimized) and allows to remove or amplify the existing vibrato. Then, Vibra Voice makes it possible to add a joint modulation of pitch, volume and timbre, artificially producing the perception of a vibrato in the voice.  

Each note sung is analyzed according to several aspects: In time, to detect the attacks, the beginnings and the end of phrases (after a breath), the changes of notes and the volume envelope. In Frequency, the pitches of the notes are estimated as well as their variations on the coma scale allowing the estimation and the follow-up of the vibrato (the tiny modulation of the pitch of the voice). 

These analysis parameters are used to control the cancellation and amplification of existing vibrato by direct transposition in phase opposition. They also allow the control of an oscillator of the type {LFO + envelope}, triggered at each note or each beginning of a phrase (after a rest). This oscillator whose frequency and amplitude change over time modulates the transposition (vibrato), the gain (tremolo) and the spectral envelope (timbrolo) parameters, applied by SuperVP. Different settings of these operators make it possible to produce the perception of a natural vibrato in the voice, as well as other original effects (slow frequency and amplitude modulations, phase shift between transposition and spectral envelope, etc.). The most important parameters are provided with random generators adding a “human” variation of the vibrato generated and thus accentuating the degree of realism. Integrated into Live, each parameter can be automated over time, in sequencer mode or in performance mode.

For creation in the studio or on the stage, Vibra Voice allows you to add lyricism to a recto-tonal voice that can go as far as producing a Bel Canto effect. Vibra Voice uses SuperVP technology which allows the pitch transposition of a high quality monodic source. The absence of the “vocoder effect” allows Vibra Voice to bring naturalness and life back to a voice in a wide variety of musical styles.

# Version and acknowledgments:
- Author: Greg Beller
- Date: 09-01-2021
- Version 1.0
- **Credits:**
    - Thanks to the Ircam Research teams ISMM and AnaSyn for SuperVP for Max, Yin and Pipo / MuBu.
    - This device uses the Ircam Template for Max For Live devices.
- **Dependencies:** This device uses latest versions of SuperVP for Max, Max Sound Box and Mubu.
    - yin ~ (pitch analysis), version 3/2018 (14) by Norbert Schnell after Cheveigne / Kawahara, IRCAM - Center Pompidou
    - supervp.trans ~, SuperVP for Max / MSP, version 2.18.5 (12/2019)
    - MuBu for Max, version 1.9.14, IRCAM - Center Pompidou , 07/14/2020
